#lang racket

(define (lab m n)
  (cond [(or (zero? m)(< m n))
         (let*
           ((sum 0)
          )
           (let loop ((i 0))
             (cond [= i n] (display sum)
                   [else (begin (let*
                            ((sum (+ sum i)))
                        (loop (+ i 1))))])
        ))]
        [else (+ m n)])
)
